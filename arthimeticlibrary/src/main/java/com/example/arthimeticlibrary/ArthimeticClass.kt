package com.example.arthimeticlibrary

class ArthimeticClass {

    fun addTwoNumbers (x: Double, y: Double): Double {
        return (x + y)
    }

    fun subtractTwoNumbers (x: Double, y: Double): Double {
        return (x - y)
    }

}